class CreateBookings < ActiveRecord::Migration[5.0]
  def change
    create_table :bookings do |t|
      t.belongs_to :user, index: true
      t.belongs_to :property
      t.datetime :start_time, null: false
      t.datetime :end_time, null: false
      
      t.timestamps
    end
  end
end
