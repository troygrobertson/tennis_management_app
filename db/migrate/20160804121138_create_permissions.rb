class CreatePermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :permissions do |t|
      t.integer :user_id,   null: false
      t.integer :role_id,   null: false
      t.boolean :approved,  null: false,  default: false      
      t.integer :permissible_id
      t.string  :permissible_type
      t.timestamps
    end

    add_index :permissions, [:permissible_id, :permissible_type]
  end
end
