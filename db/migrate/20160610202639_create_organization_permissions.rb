class CreateOrganizationPermissions < ActiveRecord::Migration[5.0]
  def change
    create_table :organization_permissions do |t|
      t.integer :user_id
      t.integer :organization_id
      t.boolean :admin, default: false, null: false
      t.boolean :approved
      t.timestamps
    end
  end
end
