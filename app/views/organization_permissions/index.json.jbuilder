json.array!(@organization_permissions) do |organization_permission|
  json.extract! organization_permission, :id
  json.url organization_permission_url(organization_permission, format: :json)
end
