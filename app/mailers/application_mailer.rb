class ApplicationMailer < ActionMailer::Base
  default from: 'support@jumpin.to'
  layout 'mailer'
end
