class Property < ApplicationRecord
  belongs_to :organization
  validates_presence_of :name, :organization_id
  
end
