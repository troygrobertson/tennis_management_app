class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  validates_presence_of :name, :email
  has_many :organization_permissions
  has_many :organizations, through: :organization_permissions
  accepts_nested_attributes_for :organizations
  

end
