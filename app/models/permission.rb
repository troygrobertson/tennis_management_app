class Permission < ApplicationRecord
  has_one :role
  belongs_to :user
  belongs_to  :organization
end
