class Organization < ApplicationRecord
  has_many :permissions
  has_many :users, through: :permissions, :source => :permissible, :source_type => "Permission"
  has_many :properties
 accepts_nested_attributes_for :users
  validates_presence_of :name

  def has_an_admin
      #errors.add(:base, 'must add at least one user') if self.organization_permissions.blank?
  end
end
