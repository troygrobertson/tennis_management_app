class OrganizationPermissionsController < ApplicationController
  before_action :set_organization_permission, only: [:show, :edit, :update, :destroy]

  # GET /organization_permissions
  # GET /organization_permissions.json
  def index
    @organization= Organization.find(params[:organization_id]).users
  end

  # GET /organization_permissions/1
  # GET /organization_permissions/1.json
  def show
  end

  # GET /organization_permissions/new
  def new
    @organization_permission = OrganizationPermission.new
  end

  # GET /organization_permissions/1/edit
  def edit
  end

  # POST /organization_permissions
  # POST /organization_permissions.json
  def create
    @organization_permission = OrganizationPermission.new(organization_permission_params)

    respond_to do |format|
      if @organization_permission.save
        format.html { redirect_to @organization_permission, notice: 'Organization permission was successfully created.' }
        format.json { render :show, status: :created, location: @organization_permission }
      else
        format.html { render :new }
        format.json { render json: @organization_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organization_permissions/1
  # PATCH/PUT /organization_permissions/1.json
  def update
    respond_to do |format|
      if @organization_permission.update(organization_permission_params)
        format.html { redirect_to @organization_permission, notice: 'Organization permission was successfully updated.' }
        format.json { render :show, status: :ok, location: @organization_permission }
      else
        format.html { render :edit }
        format.json { render json: @organization_permission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organization_permissions/1
  # DELETE /organization_permissions/1.json
  def destroy
    @organization_permission.destroy
    respond_to do |format|
      format.html { redirect_to organization_permissions_url, notice: 'Organization permission was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization_permission
      @organization_permission = OrganizationPermission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_permission_params
      params.require(:organization_permission).permit(:user_id,:organization_id,:admin)
    end
end
