  class Users::SessionsController < Devise::SessionsController
  prepend_before_action :require_no_authentication, only: [:new, :create]

  # GET /resource/sign_in
   def new
	self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    respond_to do |format|
      format.html {render "new"}
      format.js {render layout: false}
    end
    yield resource if block_given?
#    respond_with(resource, serialize_options(resource))
   end

  # POST /resource/sign_in
   def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in)
    sign_in(resource_name, resource)

    flash[:notice]= "Successfully signed in!"
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
=begin
    respond_with resource do |format|
      format.html {redirect_to after_sign_in_path_for(resource)}
      format.js {redirect_to after_sign_in_path_for(resource)}
      format.json{redirect_to after_sign_in_path_for(resource)}
    end
=end
   end

  # DELETE /resource/sign_out
   def destroy
     super
   end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
