class UsersController < ApplicationController
  before_action :authenticate_user!
  def index
    @user=User.all
  end
  def show
    @user = User.find(params[:id])

  end

  def create
  end

  def new
  end

  def update
    
  end

  def edit
    @user= User.find(params[:id])
  end
  def destroy
    @user = User.find(params[:id])
    if current_user.id == @user.id
      if @user.delete
        flash[:success]= "your account has been successfully deleted"
      end
    else
      flash[:alert] = "You do not have permission to delete user"
    end
    redirect_to about_us_path
  end
end
