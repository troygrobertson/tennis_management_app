class ErrorsController < ApplicationController
  #rescue_from "MyCustomErrors::AjaxAuthError", with: :unconfirmed
  def show

	@exception = request.env["action_dispatch.exception"]
    if @exception.class == MyCustomErrors::AjaxAuthError
      redirect_to new_user_confirmation_path, :flash => { :notice => @exception.message }
    else
      respond_to do |format|
        format.html { render action: request.path[1..-1] }
      end
    end
  end
 
  private

end
