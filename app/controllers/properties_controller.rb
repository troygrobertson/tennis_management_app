class PropertiesController < ApplicationController
  before_action :set_property, only: [:show, :edit, :update, :destroy]
  before_action :logged_in
  before_action :check_if_admin, only: [:index, :show, :update, :create, :edit, :destroy]

  # GET /properties
  # GET /properties.json
  def index
    @organization= Organization.find(params[:organization_id])
    @properties = Property.where(organization_id: @organization)

  end

  # GET /properties/1
  # GET /properties/1.json
  def show
  end

  # GET /properties/new
  def new
    @property = Property.new
    @organization = Organization.find(params[:organization_id])
  end

  # GET /properties/1/edit
  def edit
  end

  # POST /properties
  # POST /properties.json
  def create
    @property = Property.new(property_params)
    
    respond_to do |format|
      if @property.save
        @organization = Organization.find(params[:organization_id])
        flash[:notice] = params
        format.html { redirect_to organization_properties_path(@organization, @property), notice: 'Property was successfully created.' }
        #format.json { render :show, status: :created, location: @property }
        
      else
        
        format.html { render :new }
        #format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /properties/1
  # PATCH/PUT /properties/1.json
  def update
    respond_to do |format|
      if @property.update(property_params)
        @organization = Organization.find(params[:organization_id])
        format.html { redirect_to organization_properties_path(@organization, @property), notice: 'Property was successfully updated.' }
        
        #format.json { render :show, status: :ok, location: @property }
      else
        format.html { render :edit }
        #format.json { render json: @property.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /properties/1
  # DELETE /properties/1.json
  def destroy
    @property.destroy
    respond_to do |format|
      format.html { redirect_to organization_properties_url(organization_id: params[:organization_id]), notice: 'Property was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def logged_in
      if current_user.nil?
        flash[:alert] = "You must be logged in to see this"
        redirect_to root_path
      end
    end

    def set_property
      @property = Property.find(params[:id])
      @organization = Organization.find(params[:organization_id])
    end
    
    def check_if_admin
@permission = Permission.where(:user_id => current_user.id, :permissible_id  => params[:organization_id], :permissible_type => "Organization")
      if @permission.empty?
        flash[:notice] = "You do not have permission to do that."
        redirect_to root_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def property_params
      params.require(:property).permit(:name, :description, :organization_id)
    end
end
