module OrganizationsHelper
  def permissionPage
    if params[:view_type] == "Permission"
     "btn btn-info" 
    else
      "btn btn-default"
    end
  end
  def propertiesPage
    if params[:view_type] == "Properties"
     "btn btn-info" 
    else
      "btn btn-default"
    end

  end
end
