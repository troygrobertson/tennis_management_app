module BookingsHelper
  def week_rows(date)
    weeks(date).map do |week|
        "<tr>#{ week.map { |day| day_cell(day) }.join.html_safe }</tr>"
    end.join.html_safe
  end
  def day_cell(day)
    "<td>#{link_to "#{day.strftime("%e")}", new_organization_property_booking_path(params[:organization_id], params[:property_id], date: day )}</td>"
  end

  def weeks(date)
    first = date.beginning_of_month.beginning_of_week(:sunday)
    last = date.end_of_month.end_of_week(:sunday)
    (first..last).to_a.in_groups_of(7)
  end

end
