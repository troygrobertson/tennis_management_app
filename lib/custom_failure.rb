class CustomFailure < Devise::FailureApp

  # You need to override respond to eliminate recall
  def respond
	# We override Devise's handling to throw our own custom errors on AJAX auth failures,
    # because Devise provides no easy way to deal with them:

    if request.xhr? && warden_message == :unconfirmed
      raise MyCustomErrors::AjaxAuthError.new("Confirmation required. Check your inbox.")
    end

	# Original code:
    if http_auth?
      http_auth
    elsif warden_options[:recall]
      recall
    else
      redirect
    end
  end
end
