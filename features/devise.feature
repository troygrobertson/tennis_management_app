Feature:implementing devise
  @javascript
  Scenario: registering a new user
    Given a user is on the main page
    And he clicks the login button
    And he clicks sign up
    And he fills in all his information
    When he submits his information
    Then he should have a success message
    And the user should exist
    And should not be confirmed
