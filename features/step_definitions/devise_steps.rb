Given(/^a user is on the main page$/) do
  visit root_path
end

Given(/^he clicks the login button$/) do
  click_link "Login"
end

Given(/^he clicks sign up$/) do
  click_link "Sign Up"
end

Given(/^he fills in all his information$/) do
  @user = FactoryGirl.build(:user)
  fill_in :user_name, with: @user.name
  fill_in :user_email, with: @user.email
  fill_in :user_password, with: "foobar"
  fill_in :user_password_confirmation, with: "foobar"
end

When(/^he submits his information$/) do
  click_button "Sign up"
end

Then(/^he should have a success message$/) do
    expect(page).to have_selector('div.alert.alert-success')
end

Then(/^the user should exist$/) do
    expect(User.find_by_email(@user.email)).to_not be_nil
end

Then(/^should not be confirmed$/) do
  expect(User.find_by_email(@user.email).confirmed?).to be false
end

