require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module TennisManagementApp
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.to_prepare do
        DeviseController.respond_to :html, :json
    end
    config.autoload_paths << Rails.root.join('lib')
	config.generators do |g|
	  g.test_framework :rspec,
		:fixtures => true,
		:view_specs => false,
		:helper_specs => false,
		:routing_specs => false,
		:controller_specs => true,
		:request_specs => true
	  g.fixture_replacement :factory_girl, :dir => "spec/factories"
	end
    config.i18n.default_locale = :en
    config.exceptions_app = self.routes
    config.action_dispatch.rescue_responses["MyCustomErrors::AjaxAuthError"] = :forbidden
  end
end
