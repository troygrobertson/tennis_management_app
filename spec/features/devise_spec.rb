require 'rails_helper' 
RSpec.feature "Register as a new user", :type =>  :feature do
  before :each do
    visit about_us_path
    click_link "Login"
    click_link "Sign Up"
    
    @user = FactoryGirl.build(:user)
    fill_in :user_name, with: @user.name
    fill_in :user_email, with: @user.email
    fill_in :user_password, with: "foobar"
  end
  scenario "user registers with valid credentials", js: true do
    fill_in :user_password_confirmation, with: "foobar"
    click_button "Sign up"
    expect(page).to have_selector('div.alert.alert-success')
    expect(User.find_by_email(@user.email)).to_not be_nil
  end
  scenario "user registers with invalid credentials", js: true do
    fill_in :user_password_confirmation, with: ""
    click_button "Sign up"
    expect(page).to have_selector('#error_explanation')
  end
end
RSpec.feature "Logging in as a user", :type =>  :feature do
  before :each do
      visit root_path
  end
  scenario "user logging in with valid credentials", :js => true do
      @user = FactoryGirl.create(:user)
      @user.confirm
    click_link "Login"
      
      fill_in :user_email, with: @user.email
      fill_in :user_password, with: "foobar"
      
      click_button "Log in"

      expect(page).to have_content(@user.name)
      expect(page).to have_selector('div.alert.alert-success')
  end
  scenario "user logging in with invalid credentials" do
    click_link "Login"
    click_button "Log in"
    expect(page).to have_selector('div.alert.alert-danger')
  end
  scenario "user is logging in with unconfirmed account" do
       @user = FactoryGirl.create(:user)
      click_link "Login"
      
      fill_in :user_email, with: @user.email
      fill_in :user_password, with: "foobar"
      
      click_button "Log in"

      expect(page).to_not have_content(@user.name)
      expect(page).to have_selector('div.alert.alert-danger')
  
  end

    
end
RSpec.feature "logout", :type =>  :feature do
  scenario "user is able to log out", :js => true do
    @user = FactoryGirl.create(:user)
    @user.confirm
    login_as @user

    visit team_path
    page.click_link "#{@user.name}" 
    page.click_link "Logout" 
    expect(page).to_not have_content(@user.name)
    expect(page).to have_content("Login")
    expect(page).to have_selector('div.alert.alert-success')
  end
end


