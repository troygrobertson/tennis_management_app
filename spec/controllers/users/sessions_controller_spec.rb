require 'rails_helper' 
RSpec.describe Users::SessionsController, :type => :controller do

  before :each do
      request.env['devise.mapping'] = Devise.mappings[:user]
  end
  describe "POST create" do
    it "returns http success" do
      post :create
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET destroy" do
    it "needs to be tested"
=begin
    before(:each) do
      sign_in FactoryGirl.create(:user), scope: :user
      user = FactoryGirl.create(:user)
      allow(controller).to receive(:authenticate_user!).and_return(true)
      allow(controller).to receive(:current_user).and_return(user)

    end
    it "returns http success" do
      expect(subject.current_user).to_not be_nil
      get :destroy
      expect(response).to have_http_status(:redirect)
      expect(subject.current_user).to be_nil
    end
=end
  end

end
