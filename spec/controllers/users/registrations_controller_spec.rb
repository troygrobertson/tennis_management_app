require 'rails_helper' 

RSpec.describe Users::RegistrationsController, :type => :controller do
  before :each do
      request.env['devise.mapping'] = Devise.mappings[:user]
  end
  describe "POST create" do
    it "returns http success" do

      user = FactoryGirl.attributes_for(:user)
      post :create, params: {:user => user}
      expect(response).to have_http_status(:redirect)
    end
  end
  describe "GET new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET destroy" do
    before(:each) do
      login_with FactoryGirl.create(:user) 
    end
    it "returns http success" do
      expect { get :destroy }.to change(User, :count).by(-1)
      expect(response).to have_http_status(:redirect)
    end
  end

end
