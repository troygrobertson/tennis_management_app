require 'rails_helper' 

RSpec.feature "Register as a new user", :type =>  :feature do
    before :each do
    visit about_us_path
    click_link "Login"
    click_link "Sign Up"
    @user = FactoryGirl.build(:user)
    fill_in :user_name, with: @user.name
    fill_in :user_email, with: @user.email
    fill_in :user_password, with: "foobar"
  end
  it "user registers with valid credentials", js: true do
    fill_in :user_password_confirmation, with: "foobar"
    click_button "Sign up"
    expect(page).to have_selector('div.alert.alert-success')
    user=User.find_by_email(@user.email)
    expect(user).to_not be_nil
    expect(user.unconfirmed_email).to be_nil
  end
  scenario "user registers with invalid credentials", js: true do
    fill_in :user_password_confirmation, with: ""
    click_button "Sign up"
    expect(page).to have_selector('#error_explanation')
  end
end
RSpec.describe Users::ConfirmationsController, :type => :controller do
  it "needs to be tested"
=begin
  before :each do
      request.env['devise.mapping'] = Devise.mappings[:user]
  end
  describe "POST create" do
    it "returns http success" do
      user= FactoryGirl.create(:user)
      post :create, params: {:confirmation_token => user.confirmation_token}
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
  describe "GET destroy" do
    before(:each) do
      login_with FactoryGirl.create(:user) 
    end
    it "returns http success" do
      expect { get :destroy }.to change(User, :count).by(-1)
      expect(response).to have_http_status(:redirect)
    end
  end
=end
end
