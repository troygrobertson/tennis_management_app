require 'faker'
FactoryGirl.define do
  factory :organizationPermission do 
    organization
    user
    admin :false
  end
end
