require 'faker'
FactoryGirl.define do
  sequence(:property_name) { |n| "Example User#{n}"}
  factory :property do
    name :property_name
    description "Lorem ipsum"
    organization
  end

end
