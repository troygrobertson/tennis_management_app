require 'faker'
FactoryGirl.define do
  sequence(:organization_name){|n| "Example Organization #{n}"}
  factory :organization do 
    name :organization_name
    
  end
end
