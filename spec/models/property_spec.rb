require 'rails_helper'

RSpec.describe Property, :type => :model do
  it "has a valid factory" do
    expect(FactoryGirl.build(:property)).to be_valid
  end
  it "requires a name to be present" do
    expect(FactoryGirl.build(:property, :name => nil)).to_not be_valid
  end
  it "requires an organization to be present" do
    expect(FactoryGirl.build(:property, :organization => nil)).to_not be_valid
  end


end
