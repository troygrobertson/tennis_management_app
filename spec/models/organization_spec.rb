require 'rails_helper'

RSpec.describe Organization, :type => :model do
  it "has a valid factory" do 
    expect(FactoryGirl.create(:organization)).to be_valid
  end
  it "requires a name to be present" do
    expect(FactoryGirl.build(:organization, :name => nil)).to_not be_valid
  end
end
